# html-table-parse

html-table-parse parses an HTML `<table>` into Python arrays or CSV or others.
It comes:

- as a command-line program, taking HTML on stdin and outputting CSV
- as a lib, exposing a few functions to parse HTML tables and convert to some formats

## License

It's licensed under [WTFPLv2](COPYING.WTFPL).
