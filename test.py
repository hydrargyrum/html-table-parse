#!/usr/bin/env pytest

# SPDX-License-Identifier: WTFPL

from pathlib import Path

import pytest
import yaml


TEST_DATA = yaml.safe_load(Path(__file__).with_name("test_data.yml").read_text())


@pytest.mark.parametrize(
	"input,dup,expected",
	TEST_DATA,
)
def test_1(input, dup, expected):
	import tableparse

	table = tableparse.parse_table(input)
	flat = tableparse.cells_to_flat(table, dup)
	got = tableparse.flat_to_csv(flat)
	got = got.replace("\r\n", "\n")
	assert got == expected
