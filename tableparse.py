#!/usr/bin/env python3

# SPDX-License-Identifier: WTFPL

from dataclasses import dataclass
from html.parser import HTMLParser
from io import StringIO
import re
import sys


# - does not handle recursive tables
# - does not handle colgroup/rowgroup

@dataclass
class Cell:
	row: int
	col: int

	rowspan: int = 1
	colspan: int = 1

	@property
	def last_row(self):
		return self.row + self.rowspan - 1

	@property
	def last_col(self):
		return self.col + self.colspan - 1

	content: str = ""

	def finalize(self):
		self.content = re.sub(r"\s{2,}", " ", self.content.strip())


class TableParser(HTMLParser):
	curcel = None

	row_limit = 10000
	col_limit = 10000

	def extend_table_to(self, nrows, ncols):
		if nrows >= self.row_limit or ncols >= self.col_limit:
			raise RuntimeError(f"new dimensions {nrows}x{ncols} exceed limits")

		if self.table and ncols > len(self.table[0]):
			for row in self.table:
				while len(row) < ncols:
					row.append(None)

		while len(self.table) < nrows:
			self.table.append([None] * ncols)

	def insert_cell(self, cell):
		for irow in range(cell.row, cell.last_row + 1):
			for icol in range(cell.col, cell.last_col + 1):
				self.table[irow][icol] = cell

	def handle_starttag(self, tag, attrs):
		attrs = dict(attrs)
		if tag == "table":
			self.table = []
			self.currentrow = -1

		elif tag == "tr":
			# <tr> does not always allocate a new row, e.g. if a previous rowspan allocated several rows.
			# Even if the rows are already filled in advance (all cols are in rowspan),
			# one must add empty <tr>s to go to the next unfilled row.
			# Else, news cols will be appended after the rowspanned columns.
			self.currentrow += 1
			if not self.table:
				self.table.append([])
				return

			assert self.table[-1], "last <tr> contained nothing?"
			self.extend_table_to(self.currentrow + 1, len(self.table[0]))

		elif tag in ("td", "th"):
			assert self.table

			irow = self.currentrow
			try:
				icol = self.table[irow].index(None)
			except ValueError:
				icol = len(self.table[irow])
				self.extend_table_to(self.currentrow + 1, icol + 1)

			# TODO: if its value is set to 0, it extends until the end of the table section
			# 1000 is a WHATWG spec limit
			rowspan = max(1, int(attrs.get("rowspan", 1)))
			if rowspan > 1000:
				rowspan = 1

			# 65534 is a WHATWG spec limit
			colspan = max(1, min(65534, int(attrs.get("colspan", 1))))

			cell = Cell(irow, icol, rowspan, colspan)

			self.extend_table_to(cell.last_row + 1, cell.last_col + 1)

			self.insert_cell(cell)
			self.curcel = cell

	def handle_data(self, data):
		if self.curcel:
			self.curcel.content += data

	def handle_endtag(self, tag):
		if tag in ("td", "th"):
			if self.curcel:
				self.curcel.finalize()
				self.curcel = None


def cells_to_prettytable(table, dupspans=False):
	from prettytable import PrettyTable

	ptable = PrettyTable()
	for irow in range(len(table)):
		row = []
		for icol in range(len(table[irow])):
			cell = table[irow][icol]
			if dupspans or (cell.row == irow and cell.col == icol):
				row.append(cell.content)
			else:
				row.append("")
		ptable.add_row(row)
	return ptable


def cells_to_flat(table, dupspans=False):
	ptable = []
	for irow in range(len(table)):
		row = []
		for icol in range(len(table[irow])):
			cell = table[irow][icol]
			if dupspans or (cell.row == irow and cell.col == icol):
				row.append(cell.content)
			else:
				row.append("")
		ptable.append(row)
	return ptable


def flat_to_csv(table, delimiter=","):
	import csv

	fp = StringIO()
	writer = csv.writer(fp, delimiter=delimiter)
	for row in table:
		writer.writerow(row)
	return fp.getvalue()


def parse_table(text):
	parser = TableParser()
	parser.feed(text)
	return parser.table


def main():
	from argparse import ArgumentParser

	argp = ArgumentParser()
	argp.add_argument(
		"--duplicate-spanned", action="store_true", default=False,
		help="When a cell spans on multiple rows/columns, duplicate the content to those spaces",
	)
	args = argp.parse_args()

	table = parse_table(sys.stdin.read())
	print(flat_to_csv(cells_to_flat(table, args.duplicate_spanned)), end="")


if __name__ == "__main__":
	main()
